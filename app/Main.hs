import GSA
import GSA.Optimisations.Folding (FoldingNodeSet, FoldingPool, foldingOptFn, succNode, getPool)

import Data.List (delete)
import Data.Maybe (fromJust)
import Data.Text (Text, unpack)
import Text.Pretty.Simple (pPrint)
import qualified Data.Map.Strict as Map
import System.Environment as SE (getArgs)
import Data.IntMap.Strict (IntMap, (!?))
import qualified Data.IntMap.Strict as IntMap
import qualified Data.Text.IO as T (readFile, writeFile)

main :: IO ()
main = do
  args <- getArgs
  parseAndFold args

parseAndFold :: [String] -> IO ()
parseAndFold (target : rest) = do
  ast <- getAST target  
  runConstantFolding ast
  parseAndFold rest
parseAndFold [] = return ()

parseAndPP :: [String] -> IO ()
parseAndPP (target : rest) = do
  ast <- getAST target  
  prettyPrint ast
  parseAndPP rest
parseAndPP [] = return ()

getAST :: String -> IO Program
getAST gsaFile = do
  t <- T.readFile gsaFile 
  print $ "Getting AST for " <> gsaFile 
  let p = case parse gsaFile t of
            Left s -> error s
            Right p' -> p'
  return p

prettyPrint :: Program -> IO ()
prettyPrint ast = do
  putStr.unpack $ programPrinter ast

runConstantFolding :: Program -> IO () 
runConstantFolding p = do
  let entryFunc = head $ Map.elems (getProgram p)
  let code = fnCode entryFunc
  -- let entryNode = getNode $ fnEntrypoint entryFunc
  let entryNode = last (IntMap.keys $ getCode code) 
  print code 
  print ""
  pPrint $ snd $ fixpoint (getCode code) succNode 
                            (foldingOptFn code) entryNode 
                                (nsempty::FoldingNodeSet) bot

debug :: IO ()
debug = do
  let a = IntMap.fromList [(1,1), (2,0), (30,0)]
  let b = IntMap.fromList []
  print $ iMapIntersect a b

iMapIntersect :: Eq a => IntMap a -> IntMap a -> IntMap a
iMapIntersect ma mb = IntMap.fromList $ IntMap.foldrWithKey f [] ma 
  where f k v b = b <> case mb !? k of 
                        Nothing -> [] 
                        Just v1 -> [(k,v) | v == v1] 

