if [ $1 = 'help' ]; then
  echo " USAGE: "
  echo " ./run.sh -d <test directory(ies)> -f <test file(s)>  --> run test files in test directories if they exist"
  echo " ./run.sh -d <test directory(ies)>                    --> run all test files in test directoires"
  echo " ./run.sh -f <test file(s)>                           --> run all test files in 'examples' directiory"
  echo " ./run.sh                                             --> run configs specified in this run script"
elif [ -z $1 ]; then
  python3 python/run.py -d examples -f tiny
else
  python3 python/run.py "$@"
fi
