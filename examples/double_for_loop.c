int f (int A[1000], int B[1000]) {
    for (int i = 0; i < 1000; i++) {
        if (i % 2) {
            A[i] = B[i];
        }
    }
    for (int i = 0; i < 1000; i++) {
        if (! (i % 2)) {
            A[i] = B[i];
        }
    }
    return A[100];
}
