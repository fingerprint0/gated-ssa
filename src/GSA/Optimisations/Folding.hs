module GSA.Optimisations.Folding where

import GSA.Types
import GSA.Kildall
import Data.Int (Int64)
import qualified Data.Int as Int
import Data.IntSet (IntSet)
import qualified Data.IntSet as IntSet
import Data.IntMap.Strict (IntMap, (!?))
import qualified Data.IntMap.Strict as Map

succNode :: Instruction -> [Int]
succNode (Inop n) = [getNode n]
succNode (Iop _ _ _ n) = [getNode n]
succNode (Icond _ _ dst1 dst2) = [getNode dst1, getNode dst2]  
succNode (Imerge _ dst1) = [getNode dst1]
succNode (Ireturn _) = []
succNode instr = error $ "Successor for node for " <> show instr <> " not implemented."

-- FoldingNodeSet is the a set of nodes that still need to be checked for optimisations
-- in DSSState, the dssWorklist holds just node numbers (Int) and dssAval holds the mapping from 
-- Node -> Optimising pool (semi-lattice)
newtype FoldingNodeSet = FoldingNodeSet 
  { getSet :: IntSet }
  deriving Show

instance NodeSet FoldingNodeSet where 
  nsempty = 
    FoldingNodeSet IntSet.empty
  nsadd node ns = 
    FoldingNodeSet (IntSet.insert node (getSet ns))
  nspick ns = 
    if IntSet.null (getSet ns) then
      Nothing
    else
      Just (IntSet.findMin (getSet ns), FoldingNodeSet (IntSet.deleteMin (getSet ns)))
  nsallNodes cfg = 
    FoldingNodeSet (Map.keysSet cfg)

-- FoldingPool is the optimising pool which in our case is the semi lattice which is a set of
-- tuples. Each tuple is a (Register, value) pair which holds the constant values assigned to
-- registers. As Reg here is stored as Int, we use an IntMap instead of a Set.
-- The value stored is parameterised as it can be of different types, Int, Float, Double etc.

data ImmTypes = 
  ImTInt Int 
  | ImTInt64 Int64 
  | ImTFloat Float 
  | ImTDouble Double 
  deriving (Eq, Ord, Show)

instance Num ImmTypes where 
  (-) (ImTInt v1) (ImTInt v2) = ImTInt ((-) v1 v2)
  (+) (ImTInt v1) (ImTInt v2) = ImTInt ((+) v1 v2)
  (*) (ImTInt v1) (ImTInt v2) = ImTInt ((*) v1 v2)
  negate (ImTInt v1) = ImTInt (-v1)

newtype  FoldingPool = FoldingPool 
  { getPool :: IntMap ImmTypes }
  deriving (Eq, Show)

instance SemiLattice FoldingPool where
  (=|>) lt0 lt1 = 
    getPool lt0 `Map.isSubmapOf` getPool lt1
  (=|=) lt0 lt1 = 
    -- FoldingPool $ Map.intersection (getPool lt0) (getPool lt1)
    FoldingPool $ Map.fromList $ Map.foldrWithKey f [] (getPool lt0) 
          where f k v b = b <> case getPool lt1 !? k of
                                Nothing -> []
                                Just v1 -> [(k,v) | v == v1]
  bot = 
    FoldingPool Map.empty
  
-- Optimising function checks all the nodes in the given lattice and checks if the current
-- instruction node assigns a value to the Reg or not
-- If it does update that tuple with the value / if it's an expression then check if the parts of
-- the expression exist in any of the other elements in the lattice 
foldingOptFn :: Code -> Int -> FoldingPool -> FoldingPool
foldingOptFn code node lt = 
  case Map.lookup node $ getCode code of
    Nothing -> error $ "Invalid node encountered " <> show node
    Just instr -> updatePoolEntry lt instr

updatePoolEntry :: FoldingPool -> Instruction  -> FoldingPool
updatePoolEntry lt (Inop _) = lt
updatePoolEntry lt (Imerge _ _) = lt
updatePoolEntry lt (Icond _ _ _ _) = lt
updatePoolEntry lt (Iop (Ointconst imm) [] dst _) = 
  FoldingPool $ Map.insert (getReg dst) (ImTInt imm) (getPool lt)
updatePoolEntry lt (Iop Omove [src] dst _) =
  case getPool lt !? getReg src of
    Nothing -> lt
    Just val -> FoldingPool $ Map.insert (getReg dst) val (getPool lt)
updatePoolEntry lt (Iop Osub [src1, src2] dst _) = 
  case getPool lt !? getReg src1 of
    Nothing -> lt
    Just val1 -> 
      case getPool lt !? getReg src2 of 
        Nothing -> lt
        Just val2 -> 
          FoldingPool $ Map.insert (getReg dst) (val1 - val2) (getPool lt)
updatePoolEntry _ op = error $ "Unhandled operation " <> show op 

lookupError :: Int -> String
lookupError i = "Could not find entry in pool for reg " <> show i


