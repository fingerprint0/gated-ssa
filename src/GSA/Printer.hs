{-# LANGUAGE OverloadedStrings #-}

module GSA.Printer (regPrinter, operationPrinter, instructionPrinter, codePrinter, 
                    functionPrinter, programPrinter) where

import Data.IntMap.Strict (IntMap)
import qualified Data.IntMap.Strict as IMap
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Text (Text)
import qualified Data.Text as T
import GSA.Types

tshow :: Show a => a -> Text
tshow = T.pack . show

parens :: Text -> Text
parens t = "(" <> t <> ")"

braces :: Text -> Text
braces t = "{" <> t <> "}"

sep :: Text -> [Text] -> Text
sep = T.intercalate

commaSep :: [Text] -> Text
commaSep = sep ", "

optionalPrinter :: (a -> Text) -> Maybe a -> Text
optionalPrinter f (Just val) = f val
optionalPrinter f Nothing = ""

regPrinter :: Reg -> Text
regPrinter (Reg i) = "x" <> tshow i

gotoPrinter :: Int -> Text 
gotoPrinter i = "goto " <> tshow i

operationPrinter :: Operation -> [Reg] -> Text
operationPrinter Omove [r1] = regPrinter r1
operationPrinter (Ointconst imm) [] = tshow imm
operationPrinter (Oshrximm imm) [r1] = regPrinter r1 <> " >>x " <> tshow imm
operationPrinter Osub [r1, r2] = regPrinter r1 <> " - " <> regPrinter r2
operationPrinter p _ = error $ "Operation printing not implemented: " <> show p

comparatorPrinter :: Comparison -> Text
comparatorPrinter Ceq = "=="
comparatorPrinter Cne = "!="
comparatorPrinter Clt = "<"
comparatorPrinter Cle = "<="
comparatorPrinter Cgt = ">"
comparatorPrinter Cge = ">="

conditionPrinter :: Condition -> [Reg] -> Text
conditionPrinter (Ccomp comparator) [r0, r1] =
  regPrinter r0 <> " " <> comparatorPrinter comparator <> "s " <> regPrinter r1 
conditionPrinter (Ccompimm comparator imm) [r0] =
  regPrinter r0 <> " " <> comparatorPrinter comparator <> "s " <> tshow imm
conditionPrinter (Ccompu comparator) [r0, r1] =
  regPrinter r0 <> " " <> comparatorPrinter comparator <> "u " <> regPrinter r1 
conditionPrinter (Ccompuimm comparator imm) [r0] =
  regPrinter r0 <> " " <> comparatorPrinter comparator <> "u " <> tshow imm
conditionPrinter cond _ = error $ "Condition printing not implemented: " <> show cond 

predPrinter :: Predicate -> Text
predPrinter Ptrue = "t" 
predPrinter Pfalse = "⊥" 
predPrinter (Plit (True, (cond, regList))) = parens (conditionPrinter cond regList)
predPrinter (Plit (False, (cond, regList))) = "¬" <> parens (conditionPrinter cond regList)
predPrinter (Pand predLeft predRight) = 
  parens (predPrinter predLeft <> "∧" <> predPrinter predRight) 
predPrinter (Por predLeft predRight) = 
  parens (predPrinter predLeft <> "∨" <> predPrinter predRight) 

predicatePrinter :: (Predicate, Reg) -> Text
predicatePrinter (pred', reg) = parens (predPrinter pred'  <> "," <> regPrinter reg)

mergePrinter :: MergeInstruction -> Text
mergePrinter (Igamma predicateList destReg) = 
  regPrinter destReg <> " = " <> "γ" <> parens (commaSep (map predicatePrinter predicateList)) 
mergePrinter (Imu reg1 reg2 destReg) =
  regPrinter destReg <> " = " <> "μ" <> parens (regPrinter reg1 <> ", " <> regPrinter reg2)
mergePrinter (Ieta predicate reg destReg) =
  regPrinter destReg <> " = " <> "η" <> predicatePrinter (predicate, reg)

instructionPrinter :: Int -> Instruction -> Text
instructionPrinter i (Inop (Node n))
  | i - 1 == n = "   " <> tshow i <> ":\tnop"
  | otherwise = "   " <> tshow i <> ":\tgoto " <> tshow n
instructionPrinter i (Iop op args dest (Node n)) =
  "   " <> tshow i <> ":\t" <> regPrinter dest <> " = " <> operationPrinter op args
    <> if i - 1 == n then "" else "goto " <> tshow n
instructionPrinter i (Icond cond args (Node dest1) (Node dest2)) = 
  "   " <> tshow i <> ":\t" <> "if " <> parens (conditionPrinter cond args) <> " " <> 
    gotoPrinter dest1 <> " else " <> gotoPrinter dest2
instructionPrinter i (Imerge instrList (Node _)) = 
  "   " <> tshow i <> ":\t" <> sep "\n    " (map mergePrinter instrList) <> "\n    " <> "nop"
instructionPrinter i (Ireturn maybeReg) = 
  "   " <> tshow i <> ":\t" <> "return " <> optionalPrinter regPrinter maybeReg
instructionPrinter _ op = error $ "Instruction printing not implemented: " <> show op

codePrinter :: Code -> Text
codePrinter (Code c) = IMap.foldrWithKey f "" c
  where
    f k a b = b <> instructionPrinter k a <> "\n"

functionPrinter :: Text -> Function -> Text
functionPrinter n f =
  n
    <> parens (commaSep $ regPrinter <$> fnParams f)
    <> " "
    <> braces ("\n" <> codePrinter (fnCode f))

programPrinter :: Program -> Text
programPrinter (Program p) = Map.foldrWithKey f "" p
  where
    f k a b = b <> functionPrinter k a <> "\n\n"
