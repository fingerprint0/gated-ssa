module GSA.Kildall where

import Debug.Trace
import Data.Maybe (fromMaybe)
import Data.IntMap.Strict (IntMap, (!?))
import qualified Data.IntMap.Strict as Map

type IntMapD a = (a, IntMap a)

class (Eq a, Show a) => SemiLattice a where
  (=|>) :: a -> a -> Bool
  (=|=) :: a -> a -> a
  bot :: a

class SemiLattice a => SemiLatticeTop a where
  top :: a

{-class DataflowSolver b where
  fixpoint ::
    SemiLattice b
    => IntMap a
    -> (a -> [Int])
    -> (Int -> b -> b)
    -> Int
    -> b
    -> Maybe (IntMapD b)

class BackwardsDataflowSolver a where
  bfixpoint ::
    SemiLattice b
    => IntMap a
    -> (a -> [Int])
    -> (Int -> b -> b)
    -> Maybe (IntMapD b)-}

class Show a => NodeSet a where
  nsempty :: a
  nsadd :: Int -> a -> a
  nspick :: a -> Maybe (Int, a)
  nsallNodes :: IntMap b -> a

data DSState lt ns = DSState
  { dssAval :: IntMap lt
  , dssWorklist :: ns
  , dssVisited :: IntMap Bool
  }

abstrValue :: SemiLattice lt => Int -> DSState lt ns -> lt
abstrValue i d = fromMaybe bot $ dssAval d !? i

propagateSucc :: (SemiLattice lt, NodeSet ns) => DSState lt ns -> lt -> Int -> DSState lt ns
propagateSucc s out n =
  case dssAval s !? n of
    Nothing -> DSState { dssAval = Map.insert n out (dssAval s)
                       , dssWorklist = nsadd n (dssWorklist s)
                       , dssVisited = Map.insert n True (dssVisited s) }
    Just oldl ->
      let newl = oldl =|= out in
      if oldl == out
      then s
      else DSState { dssAval = Map.insert n newl (dssAval s)
                   , dssWorklist = nsadd n (dssWorklist s)
                   , dssVisited = Map.insert n True (dssVisited s) }

propagateSuccList :: (SemiLattice lt, NodeSet ns) => DSState lt ns -> lt -> [Int] -> DSState lt ns
propagateSuccList s _ [] = s
propagateSuccList s out (n : r) = propagateSuccList (propagateSucc s out n) out r

step ::
  (SemiLattice lt, NodeSet ns)
  => IntMap a -- ^ code
  -> (a -> [Int]) -- ^ successors
  -> (Int -> lt -> lt) -- ^ transfer
  -> DSState lt ns
  -> Either (IntMapD lt) (DSState lt ns)
step code successors transf s =
  case nspick (dssWorklist s) of
    Nothing -> Left (bot, dssAval s)
    Just (n, r) ->
      case code !? n of
        Nothing -> Right (s { dssWorklist = r })
        Just instr ->
          Right (propagateSuccList (s { dssWorklist = r }) 
                                    (transf n (abstrValue n s)) (successors instr))

iter ::
  (SemiLattice lt, NodeSet ns)
  => IntMap a
  -> (a -> [Int])
  -> (Int -> lt -> lt)
  -> DSState lt ns
  -> IntMapD lt
iter code suc transf s =
  case step code suc transf s of
    Left res -> res
    Right s' -> iter code suc transf s'

startState :: (NodeSet ns) => Int -> ns -> lt -> DSState lt ns
startState enode empty eval =
  DSState { dssAval = Map.insert enode eval Map.empty
          , dssWorklist = nsadd enode empty
          , dssVisited = Map.insert enode True Map.empty }

fixpoint ::
  (SemiLattice lt, NodeSet ns)
  => IntMap a
  -> (a -> [Int])
  -> (Int -> lt -> lt)
  -> Int
  -> ns
  -> lt
  -> IntMapD lt
fixpoint code suc transf enode empty eval =
  iter code suc transf (startState enode empty eval)
