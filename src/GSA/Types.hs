module GSA.Types where

import Data.IntMap.Strict (IntMap)
import Data.Map.Strict (Map)
import Data.Text (Text)
import Data.Int (Int64)

data Typ
  = Tint
  | Tfloat
  | Tlong
  | Tsingle
  | Tany32
  | Tany64
  deriving (Eq, Show)

data RetTyp
  = Tret Typ
  | Tint8signed
  | Tint8unsigned
  | Tint16signed
  | Tint16unsigned
  | Tvoid
  deriving (Eq, Show)

data Signature = Signature
  { sigArgs :: [Typ],
    sigRes :: RetTyp
  }
  deriving (Eq, Show)

data Chunk
  = Mint8signed
  | Mint8unsigned
  | Mint16signed
  | Mint16unsigned
  | Mint32
  | Mint64
  | Mfloat32
  | Mfloat64
  | Many32
  | Many64
  deriving (Eq, Show)

signatureMain :: Signature
signatureMain = Signature {sigArgs = [], sigRes = Tret Tint}

newtype Node = Node {getNode :: Int} deriving (Eq, Show)

newtype Reg = Reg {getReg :: Int} deriving (Eq, Show)

newtype Ident = Ident {getIdent :: Int} deriving (Eq, Show)

newtype Ptrofs = Ptrofs { getPtrofs :: Int } deriving (Eq, Show)

data Comparison =
  Ceq
  | Cne
  | Clt
  | Cle
  | Cgt
  | Cge
  deriving (Eq, Show)

negateComparison Ceq = Cne
negateComparison Cne = Ceq
negateComparison Clt = Cge
negateComparison Cle = Cgt
negateComparison Cgt = Cle
negateComparison Cge = Clt

swapComparison Ceq = Ceq
swapComparison Cne = Cne
swapComparison Clt = Cgt
swapComparison Cle = Cge
swapComparison Cgt = Clt
swapComparison Cge = Cle

data Condition =
  Ccomp Comparison
  | Ccompu Comparison
  | Ccompimm Comparison Int
  | Ccompuimm Comparison Int
  | Ccompl Comparison
  | Ccomplu Comparison
  | Ccomplimm Comparison Int64
  | Ccompluimm Comparison Int64
  | Ccompf Comparison
  | Cnotcompf Comparison
  | Ccompfs Comparison
  | Cnotcompfs Comparison
  | Cmaskzero Int
  | Cmasknotzero Int
  deriving (Eq, Show)

data Addressing =
  Aindexed Integer
  | Aindexed2 Integer
  | Ascaled Integer Integer
  | Aindexed2scaled Integer Integer
  | Aglobal Ident Ptrofs
  | Abased Ident Ptrofs
  | Abasedscaled Integer Ident Ptrofs
  | Ainstack Ptrofs
  deriving (Eq, Show)

data Operation = Omove
  | Ointconst Int
  | Olongconst Int64
  | Ofloatconst Double
  | Osingleconst Float
  | Oindirectsymbol Ident
  | Ocast8signed
  | Ocast8unsigned
  | Ocast16signed
  | Ocast16unsigned
  | Oneg
  | Osub
  | Omul
  | Omulimm Int
  | Omulhs
  | Omulhu
  | Odiv
  | Odivu
  | Omod
  | Omodu
  | Oand
  | Oandimm Int
  | Oor
  | Oorimm Int
  | Oxor
  | Oxorimm Int
  | Onot
  | Oshl
  | Oshlimm Int
  | Oshr
  | Oshrimm Int
  | Oshrximm Int
  | Oshru
  | Oshruimm Int
  | Ororimm Int
  | Oshldimm Int
  | Olea Addressing
  | Omakelong
  | Olowlong
  | Ohighlong
  | Ocast32signed
  | Ocast32unsigned
  | Onegl
  | Oaddlimm Int64
  | Osubl
  | Omull
  | Omullimm Int64
  | Omullhs
  | Omullhu
  | Odivl
  | Odivlu
  | Omodl
  | Omodlu
  | Oandl
  | Oandlimm Int64
  | Oorl
  | Oorlimm Int64
  | Oxorl
  | Oxorlimm Int64
  | Onotl
  | Oshll
  | Oshllimm Int
  | Oshrl
  | Oshrlimm Int
  | Oshrxlimm Int
  | Oshrlu
  | Oshrluimm Int
  | Ororlimm Int
  | Oleal Addressing
  | Onegf
  | Oabsf
  | Oaddf
  | Osubf
  | Omulf
  | Odivf
  | Onegfs
  | Oabsfs
  | Oaddfs
  | Osubfs
  | Omulfs
  | Odivfs
  | Osingleoffloat
  | Ofloatofsingle
  | Ointoffloat
  | Ofloatofint
  | Ointofsingle
  | Osingleofint
  | Olongoffloat
  | Ofloatoflong
  | Olongofsingle
  | Osingleoflong
  | Ocmp Condition
  | Osel Condition Typ
  deriving (Eq, Show)

data Pred a
  = Ptrue
  | Pfalse
  | Plit (Bool, a)
  | Pand (Pred a) (Pred a)
  | Por (Pred a) (Pred a)
  deriving (Eq, Show)

type Predicate = Pred (Condition, [Reg])

data MergeInstruction
  = Imu Reg Reg Reg
  | Igamma [(Predicate, Reg)] Reg
  | Ieta Predicate Reg Reg
  deriving (Eq, Show)

data Instruction
  = Inop Node
  | Iop Operation [Reg] Reg Node
  | Iload Chunk Addressing [Reg] Reg Node
  | Istore Chunk Addressing [Reg] Reg Node
  | Icall Signature (Either Reg Ident)
  | Icond Condition [Reg] Node Node
  | Ijumptable Reg [Node]
  | Imerge [MergeInstruction] Node
  | Ireturn (Maybe Reg)
  deriving (Eq, Show)

newtype Code = Code {getCode :: IntMap Instruction} deriving (Eq, Show)

data Function = Function
  { fnSig :: Signature,
    fnParams :: [Reg],
    fnStackSize :: Int,
    fnCode :: Code,
    fnEntrypoint :: Node,
    fnExtParams :: [Reg]
  }
  deriving (Eq, Show)

newtype Program = Program {getProgram :: Map Text Function} deriving (Eq, Show)
