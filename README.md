# GSA Parser

## What is this repo?
  * Writing a parser for GSA (Gated-SSA) IR to explore it as an expansion to SSA
  * Implementing constant folding using Kildall's Algorithm 
  * Collaborative 

## Run Instructions

Use run.sh to execute the parser.
Usage guidelines:
```sh
./run.sh help
```

## Build Instructions

First, install nix. This is a bit of an effort on a docker image.
The latest version of ymherklotz/polyhedral:1.0 has some of the config files you get when installing nix so everyone has it and if you kill a container you still have your configurations.
But nix is big so we need to use docker volumes and install it once with the follwing steps.

Create a volume that will be mounted to /nix.
It's important it's always mounted to /nix cos that's where nix gets installed. 
```sh
docker volume create nix 
docker run -itd --name gsa-parser -v nix:/nix -v <project_path_on_host>:<project_path_on_image> ymherklotz/polyhedral:1.0
```

Create a temporary user as whom we install nix (it doesn't allow installation as root). 
The final command installs multi-user nix which allows root to run nix after installation.
``` sh
apt install curl bzip2 adduser
adduser --disabled-password --gecos '' nix-installer
chown nix-installer /nix
sudo -u nix-installer /bin/bash
cd /home/nix-installer
sh <(curl -L https://nixos.org/nix/install) --daemon
```

It's recommended to use `flake` in nix, which is experimental:

``` sh
nix-env -iA nixpkgs.nixUnstable
```

Then add the following to `/etc/nix/nix.conf`:

``` text
experimental-features = nix-command flakes
```

For faster compilation, use [cachix](https://www.cachix.org/) to download binary dependencies:

``` sh
nix-env -iA cachix -f https://cachix.org/api/v1/install
```

Then, activate the following caches:

``` sh
USER=root cachix use jmc 
USER=root cachix use ymherklotz
```

Then navigate to this project's directory and build the project using:

``` sh
nix build
```
