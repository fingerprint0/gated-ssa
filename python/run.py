import sys
import argparse
import subprocess

from pathlib import Path

def parse_command_line_args():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--dirs', '-d', nargs='*', type=str, default=None, help='target directories')
    parser.add_argument('--files', '-f', nargs='*', type=str, default=None, help='target files')
    args = parser.parse_args()
    return args

def main(args):
    if args.dirs == None:    targDir = [Path('examples')]
    else:                   targDir = [Path(direc) for direc in args.dirs]

    for d in targDir:
        if args.files == None:  targFiles = [f.stem for f in d.iterdir() if f.is_file()]
        else:                   targFiles = args.files

        for f in d.iterdir():
            if f.is_file() and f.match('*.gsa') and (f.stem in targFiles):
                subprocess.run(f'cabal run gsa-parser-exe -- {f}', shell=True)

if __name__ == "__main__":
    args = parse_command_line_args()
    main(args)
